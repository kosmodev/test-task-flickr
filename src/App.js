import React, { Component } from 'react';
import HeaderSearch from "./components/SearchScreen"
import Gallery from "./components/Gallery"
import { BrowserRouter as Router, Route } from 'react-router-dom';

class App extends Component {
	render() {
		return (
			<Router>
				<div>
					<Route exact path="/" component={HeaderSearch} />
					<Route path="/gallery/:query" component={Gallery} />
				</div>
			</Router>
		);
	}
}

export default App;