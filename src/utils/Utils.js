class Utils {
	static resizable(el, factor) {
		const int = Number(factor) || 5.0;

		function resize() {
			el.style.width = ((el.value.length + 1) * int) + 'px'
		}

		const e = 'keyup,keypress,focus,blur,change'.split(',');
		for (const i in e) el.addEventListener(e[i], resize, false);
		resize();
	}
}

export default Utils;