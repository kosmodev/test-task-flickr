import React, {Component} from 'react';
import foundBt from "../images/group-3.svg"
import "../css/gallery.css"
import "../css/mobile/galleryMob.css"
import logo from "../images/bitmap.png"
import utils from "../utils/Utils"
import GalleryGrid from "./GalleryGrid"
import Info from "./Info"
import axios from 'axios';

class Gallery extends Component {

	constructor(props) {
		super(props);
		this.state = {
			inputValue: '',
			total: 0,
			photos: []
		};
	}

	_handleKeyPress = (e) => {
		if (e.key === 'Enter') {
			this.setState({
				total: 0,
				photos: []
			});
			this.loadPhotosByTag(this.state.inputValue);
		}
	};

	loadPhotosByTag(txt) {
		let url = 'https://api.flickr.com/services/rest/' +
			'?method=flickr.photos.search&api_key=273e2ea2636e2f4cedecb6008be40fd2' +
			'&tags=' + txt +
			'&format=json&nojsoncallback=1';
		axios.get(url)
			.then((response) => {
				console.log("response.data",response.data);
				this.setState({
					total: response.data.photos.total,
					photos: response.data.photos.photo,
					perpage: response.data.photos.perpage
				});
			})
			.catch((error) => {
				console.log(error);
			});
	}

	updateInputValue(evt) {
		this.setState({
			inputValue: evt.target.value
		});
	}

	componentDidMount() {
		this.setState({
			inputValue: this.props.match.params.query
		});
		let query = this.props.match.params.query;

		var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
		if (!isMobile) {
			utils.resizable(document.getElementById('txt'), 15);
		}

		this.loadPhotosByTag(query);
	}

	onItemClick(event) {
		document.getElementById("txt").focus();
	}

	render() {
		let content;


		if (!this.state.inputValue) {
			content = <Info bgTxt={"Search"} txt1={"No Result:"}
							txt2={"Write any word to start search"} txt3={""}/>;
		} else if (this.state.photos.length === 0) {
			content = <Info bgTxt={"OOPS"} txt1={"No Result for:"}
							txt2={this.state.inputValue} txt3={"Try another word."}/>;
		}
		if (this.state.photos.length > 0) {
			content = <GalleryGrid photos={this.state.photos}/>;
		}

		return (
			<div className="galleryRoot">
				{content}
				<div className="headerGallery">
					<img srcSet={logo} className="logo"/>
					<div className="searchGroup">
						<img srcSet={foundBt}
							 onClick={this.onItemClick}
							 className="iconDandruff"/>
						<input id="txt" value={this.state.inputValue}
							   className="Search-Gallery"
							   onKeyPress={this._handleKeyPress}
							   onChange={evt => this.updateInputValue(evt)}/>
					</div>

					<div className="countFound">
						<span className="result">Result:</span>
						<span className="text">{this.state.photos.length}</span>
						<span className="result">photos</span>
					</div>
				</div>
			</div>
		);
	}
}

export default Gallery;