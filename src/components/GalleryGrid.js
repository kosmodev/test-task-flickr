import React, {Component} from 'react';
import "../css/galleryGrid.css"
import "../css/mobile/galleryGridMob.css"

class GalleryGrid extends Component {
	constructor(props) {
		super(props);

		this.state = {
			inputValue: ''
		};
	}

	render() {
		let photos = this.props.photos;
		return (
			<div className="photoContener">
				<div className="masonry">
					{photos.map(x => (<div className="item">
						<img srcSet={"https://farm"+x.farm+".staticflickr.com/"
						+x.server+"/"+x.id+"_"+x.secret+"_n.jpg"} />
					</div>))}
				</div>
			</div>
		);
	}

	updateInputValue(evt) {
		this.setState({
			inputValue: evt.target.value
		});
	}
}

export default GalleryGrid;