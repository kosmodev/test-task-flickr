import React, {Component} from 'react';
import "../css/info.css"
import "../css/mobile/infoMob.css"

class GalleryGrid extends Component {
	constructor(props) {
		super(props);
		this.state = {
			inputValue: ''
		};
	}

	_handleKeyPress = (e) => {
		if (e.key === 'Enter') {
			 this.props.history.push("/gallery/"+this.state.inputValue)
		}
	};

	render() {
		return (
			<div className="infoMain">
				<span className="OOPS_info">{this.props.bgTxt}</span>
				<div className="info">
					<h3 className="result_info">{this.props.txt1}</h3>
					<h1 className="result_info">{this.props.txt2}</h1>
					<h3 className="Try-another-word_info">{this.props.txt3}</h3>
				</div>
			</div>
		);
	}

	updateInputValue(evt) {
		this.setState({
			inputValue: evt.target.value
		});
	}
}

export default GalleryGrid;