import React, {Component} from 'react';
import "../css/header.css"
import logo from "../images/bitmap.png"
class SearchScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			inputValue: ''
		};
	}

	_handleKeyPress = (e) => {
		if (e.key === 'Enter') {
			 this.props.history.push("/gallery/"+this.state.inputValue)
		}
	};

	render() {
		return (
			<div className="header">
			<header>
				<img srcSet={logo} className="Bitmap"/>
				<div className="Line"/>
				<input value={this.state.inputValue}
					   className="Type-to-Search"
					   placeholder="Tap to search"
					   onKeyPress={this._handleKeyPress}
					   onChange={evt => this.updateInputValue(evt)}/>
			</header>
			</div>
		);
	}

	updateInputValue(evt) {
		this.setState({
			inputValue: evt.target.value
		});
	}
}

export default SearchScreen;